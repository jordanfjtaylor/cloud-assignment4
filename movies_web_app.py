import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movie_data(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT UNSIGNED NOT NULL, title TEXT, director TEXT, actor TEXT, date TEXT, rating INT UNSIGNED, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

@app.route('/add_movie', methods=['POST'])
def add_to_db():
    print("Received request.")

    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)


    cur = cnx.cursor()
    commands = "SELECT title FROM movie_data WHERE UPPER(title) LIKE UPPER(%s)"
    values = (title,)

    cur.execute(commands, values)
    query = cur.fetchall()
    if(len(query) > 0):
        message = message = "Movie " + str(title) + " could not be inserted: already exists"
        return render_template('index.html', message=message)



    cur = cnx.cursor()
    commands = "INSERT INTO movie_data (year, title, director, actor, date, rating) values (%s, %s, %s, %s, %s, %s)"
    values = (year, title, director, actor, date, rating)
    try:
        cur.execute(commands, values)
        cnx.commit()
        message = "Movie " + str(title) + " successfully inserted"
    except Exception as exp:
        message = "Movie " + str(title) + " could not be inserted: " + str(exp)
    return render_template('index.html', message=message)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)



    cur = cnx.cursor()
    commands = "SELECT title FROM movie_data WHERE UPPER(title) like UPPER(%s)"
    values = (title,)
    cur.execute(commands, values)

    query = cur.fetchall()
    if(len(query) == 0):
        message = "Movie could not be updated: " + str(title) + " does not exist"
        return render_template('index.html', message=message)


    cur = cnx.cursor()
    commands = "UPDATE movie_data SET year = %s, director = %s, actor = %s, date = %s, rating = %s WHERE UPPER(title) LIKE UPPER(%s)"
    values = (year, director, actor, date, rating, title)

    try:
        cur.execute(commands, values)
        cnx.commit()
        message = "Movie " + str(title) + " successfully updaded"
    except Exception as exp:
        message = "Movie " + str(title) + " could not be inserted: " + str(exp)

    return render_template('index.html', message=message)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    
    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    commands = "SELECT title FROM movie_data WHERE UPPER(title) like UPPER(%s)"
    values = (title,)
    cur.execute(commands, values)

    query = cur.fetchall()
    if(len(query) == 0):
        message = "Movie with " + str(title) + " does not exist"
    else:
        commands = "DELETE FROM movie_data WHERE UPPER(title) like UPPER(%s)"
        values = (title,)
        try:
            cur.execute(commands, values)
            cnx.commit()
            message = "Movie with " + str(title) + " successfully deleted"

        except Exception as exp:
            message = "Movie " + str(title) + " could not be deleted: " + str(exp)

    return render_template('index.html', message=message)

@app.route('/search_movie', methods=['GET', 'POST'])
def search_movie():
    print(request)
    actor = request.form['search_actor']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    commands = "SELECT title, year, actor FROM movie_data WHERE UPPER(actor) like UPPER(%s)"
    values = (actor,)
    cur.execute(commands, values)
    query = cur.fetchall()
    
    message = ''
    if(len(query) == 0):
        message = "No movies found for actor " + str(actor)
    else:
        for x in query:
            message  = message + str(x[0]) + ', ' + str(x[1]) + ', ' + str(x[2]) + '\n\r' 

    return render_template('index.html', message=message)

@app.route('/highest_rating', methods=['GET', 'POST'])
def get_highest():


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    commands = "SELECT title, year, actor, director, rating from movie_data where rating in (select max(rating) from movie_data)"
    cur.execute(commands)
    query = cur.fetchall()
    
    message = ''
    if(len(query) == 0):
        message = "There are no movies"
    else:
        for x in query:
            message  = message + str(x[0]) + ', ' + str(x[1]) + ', ' + str(x[2]) + ', ' + str(x[3]) + ', ' + str(x[4]) + '\n'  

    return render_template('index.html', message=message)

@app.route('/lowest_rating', methods=['GET', 'POST'])
def get_lowest():


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    commands = "SELECT title, year, actor, director, rating from movie_data where rating in (select min(rating) from movie_data)"
    try:
        cur.execute(commands)
    except Exception as exp:
        return render_template('Error')
    query = cur.fetchall()
    
    message = ''
    if(len(query) == 0):
        message = "There are no movies"
    else:
        for x in query:
            message  = message + str(x[0]) + ', ' + str(x[1]) + ', ' + str(x[2]) + ', ' + str(x[3]) + ', ' + str(x[4]) + '\n' 

    return render_template('index.html', message=message)


@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print("Before displaying index.html")
    return render_template('index.html', message="Welcome!")


if __name__ == "__main__":
    app.debug = False
    app.run(host='0.0.0.0')